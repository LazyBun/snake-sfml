#include "GameLoop.h"


GameLoop::GameLoop(sf::RenderWindow* mw) : state(GameState(MENU)), mainWindow(mw), menu(mw, state)
{
	loop();
}

GameLoop::~GameLoop()
{
	delete mainWindow;
}


void GameLoop::loop()
{
	bool singlePlayerCheck = false;

	while (mainWindow->isOpen())
    {
    	mainWindow->clear(sf::Color::Black);
    	if(state == MENU)
    	{
    		
    		//handleMenuInput
    		menu.handleInput();
    		//draw menu
    		menu.draw();
			mainWindow->display();
    	}
    	else if(state == GAMESINGLE)
    	{
    		singlePlayerLoop();
    	}
    	else if(state == END)
    	{
    		break;
    	}

        // check all the window's events that were triggered since the last iteration of the loop
    }
}



void GameLoop::singlePlayerLoop()
{
	PlayerMoveSet moveSet{
		sf::Keyboard::Up,sf::Keyboard::Left,
		sf::Keyboard::Right,sf::Keyboard::Down,
		sf::Keyboard::Key::P
	};
	Pos pos{
		0,
		0
	};

	sf::Clock timer;

	Player player(state, moveSet, pos, mainWindow);
	player.initialize();
	while(mainWindow->isOpen())
	{
		mainWindow->clear(sf::Color::Black);
		if(state != GAMESINGLE)
			break;
		timer.restart();
		while(timer.getElapsedTime().asMilliseconds() < 1000) //do better loop
		{
			player.handleInput();	
		}
		player.update();
		player.draw();
		mainWindow->display();
	}
}
