#ifndef GAMELOOP_H
#define GAMELOOP_H
#include "Headers.h"
#include "Menu.h"
#include "Player.h"

class GameLoop
{
private:

void loop();
void singlePlayerLoop();
Menu menu;

public:
	sf::RenderWindow* mainWindow;
	GameState state;
	GameLoop(sf::RenderWindow* mw);
	~GameLoop();
};
#endif