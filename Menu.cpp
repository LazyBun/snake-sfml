#include "Menu.h"


//MENU OPTIONS
static std::string menuOption1 = "Single Player";
static std::string menuOption2 = "Options";
static std::string menuOption3 = "Exit";
//




Menu::Menu(sf::RenderWindow* w, GameState& s) : mainWindow(w), state(&s), pos(0)
{
	menuOptions.push_back(menuOption1);
	menuOptions.push_back(menuOption2);
	menuOptions.push_back(menuOption3);


	if (!font.loadFromFile("OpenSans-Bold.ttf"))
	{
	    // error...
	}
	{
		sf::Text text;
		sf::FloatRect textRect = text.getLocalBounds();
		text.setOrigin(textRect.left + textRect.width/2.0f,
		          	   textRect.top  + textRect.height/2.0f);
		text.setPosition(sf::Vector2f(Consts::resolutionWidth/2.0f,Consts::resolutionHeight/2.0f + (menuOptionsText.size() * (Consts::fontSize +4))));
		menuOptionsText.push_back(text);
		menuOptionsText[menuOptionsText.size()-1].setFont(font);
		menuOptionsText[menuOptionsText.size()-1].setString(menuOptions[menuOptionsText.size()-1]);
		menuOptionsText[menuOptionsText.size()-1].setCharacterSize(Consts::fontSize);
	}	

	{
		sf::Text text;
		sf::FloatRect textRect = text.getLocalBounds();
		text.setOrigin(textRect.left + textRect.width/2.0f,
		          	   textRect.top  + textRect.height/2.0f);
		text.setPosition(sf::Vector2f(Consts::resolutionWidth/2.0f,Consts::resolutionHeight/2.0f + (menuOptionsText.size() * (Consts::fontSize +4))));
		menuOptionsText.push_back(text);
		menuOptionsText[menuOptionsText.size()-1].setFont(font);
		menuOptionsText[menuOptionsText.size()-1].setString(menuOptions[menuOptionsText.size()-1]);
		menuOptionsText[menuOptionsText.size()-1].setCharacterSize(Consts::fontSize);
	}
	
	{
		sf::Text text;
		sf::FloatRect textRect = text.getLocalBounds();
		text.setOrigin(textRect.left + textRect.width/2.0f,
		          	   textRect.top  + textRect.height/2.0f);
		text.setPosition(sf::Vector2f(Consts::resolutionWidth/2.0f,Consts::resolutionHeight/2.0f + (menuOptionsText.size() * (Consts::fontSize +4))));
		menuOptionsText.push_back(text);
		menuOptionsText[menuOptionsText.size()-1].setFont(font);
		menuOptionsText[menuOptionsText.size()-1].setString(menuOptions[menuOptionsText.size()-1]);
		menuOptionsText[menuOptionsText.size()-1].setCharacterSize(Consts::fontSize);
	}

}

Menu::~Menu()
{

}


void Menu::handleInput()
{
	sf::Event event;
    while (mainWindow->pollEvent(event))
    {
    	if (event.type == sf::Event::KeyPressed)
		{
			if(event.key.code == sf::Keyboard::Up)
			{
	    		if(pos == 0)
	    			pos = menuOptions.size()-1;
	    		else
	    			pos--;
	    	}
	    	else if(event.key.code == sf::Keyboard::Down)
	    	{
	    		if(pos == menuOptions.size()-1)
	    			pos = 0;
	    		else
	    			pos++;
	    	}
	    	else if(event.key.code == sf::Keyboard::Return)
	    	{
	    		if(menuOptions[pos].compare(menuOption1) == 0)
	    		{
	    			//play single
	    			*state = GameState(GAMESINGLE);
	    		}
	    		else if(menuOptions[pos].compare(menuOption2) == 0)
	    		{
	    			//Options
	    			*state = GameState(OPTIONS);
	    		}
	    		else if(menuOptions[pos].compare(menuOption3) == 0)
	    		{
	    			//Exit
	    			*state = GameState(END);
	    		}
	    	}
		}
        // "close requested" event: we close the window
        else if (event.type == sf::Event::Closed)
            mainWindow->close();
    }
}

void Menu::draw()
{
	//TODO

	for(int i=0; i<menuOptionsText.size(); i++)
	{
		if(i == pos)
			menuOptionsText[i].setColor(sf::Color::Red);
		else
			menuOptionsText[i].setColor(sf::Color::White);
		mainWindow->draw(menuOptionsText[i]);
	}
}