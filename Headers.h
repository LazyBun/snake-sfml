#ifndef HEADERS_H
#define HEADERS_H

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include <queue>

namespace Consts
{
	float const resolutionWidth = 800;
	float const resolutionHeight = 600;
	int const fontSize = 24;
	int const mapSize = 30;
}

enum GameState
{
	MENU,
	GAMESINGLE,
	GAMEMULTI,
	END,
	OPTIONS,
	PAUSE
};

struct Pos
{
	int x,y;
};

struct PlayerMoveSet //char --> sfml
{
	sf::Keyboard::Key up, left, right, down, pause;
};

#endif