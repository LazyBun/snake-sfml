#include "Player.h"


Player::Player(GameState& gs, PlayerMoveSet pms, Pos p, sf::RenderWindow* rw) : 
				state(gs), moveSet(pms), gamePos(p), mainWindow(rw),
				border(sf::Vector2f(10,10)), head(10), tail(10), point(sf::Vector2f(10,10))
{
}

Player::~Player()
{
}

Pos Player::randomPos()
{
	//<random> change
	static std::random_device rd;
	static std::uniform_int_distribution<int> uniform_dist(1, Consts::mapSize-2);
	Pos result{uniform_dist(rd), uniform_dist(rd)};
	for(int i=0; i<snake.size(); i++)
	{
		while(result.x == snake[i].x && result.y == snake[i].y)
		{
			result = {uniform_dist(rd), uniform_dist(rd)};
			break;
		}
	}
	return result;
}

void Player::addPoint()
{
	//add protection for duplicates
	points.push_back(
		Pos{
			randomPos().x,
			randomPos().y
		}
	);
}

void Player::initialize()
{	//todo, return error?
	//
	//CHANGE MAP TO VECTORS, MAP IS USELESS
	//
	//setup snake
	snake.push_back(
		Pos{
			Consts::mapSize/2,
			Consts::mapSize/2
		}
	);
	direction = UP;
	//setup point
	points.push_back(
		Pos{
			randomPos().x,
			randomPos().y
		}
	);

}



void Player::handleInput()
{
	//inputs
	if (sf::Keyboard::isKeyPressed(moveSet.up))
	{
 	   	if(direction != DOWN)
			direction = UP;
	}
	if (sf::Keyboard::isKeyPressed(moveSet.left))
	{
	 	if(direction != RIGHT)
			direction = LEFT;   
	}
	if (sf::Keyboard::isKeyPressed(moveSet.right))
	{
	    if(direction != LEFT)
			direction = RIGHT;
	}
	if (sf::Keyboard::isKeyPressed(moveSet.down))
	{
	    if(direction != UP)
			direction = DOWN;
	}
	if (sf::Keyboard::isKeyPressed(moveSet.pause))
	{
	    while(true)
		{
			if(sf::Keyboard::isKeyPressed(moveSet.pause))
				break;
		}
	}
	//
	//window stuff
	sf::Event event;
    while (mainWindow->pollEvent(event))
    {
        // "close requested" event: we close the window
        if (event.type == sf::Event::Closed)
            mainWindow->close();
    }
    //
}

void Player::update()
{
	//add point if eaten last loop
	bool ate = false;
	if(points.size()==0)
	{
		addPoint();
		ate = true;
	}
	//move in direction
	std::queue<Pos> currentMoves;
	for(int i=0; i<snake.size(); i++)
	{
		Pos pos;
		if(i == 0)
		{
			switch(direction)
			{
				case UP:
				{
					snake[0].x--;
					break;
				}
				case DOWN:
				{
					snake[0].x++;
					break;
				}
				case LEFT:
				{
					snake[0].y--;
					break;
				}
				case RIGHT:
				{
					snake[0].y++;
					break;
				}
				default:
				break;
			}
			pos.x = snake[0].x;
			pos.y = snake[0].y;
			currentMoves.push(pos);
		}
		else
		{
			snake[i].x = moves.front().x;
			snake[i].y = moves.front().y;
			moves.pop();
			pos.x = snake[i].x;
			pos.y = snake[i].y;
			currentMoves.push(pos);
		}
	}
	//add new snake fragment if ate
	if(ate)
	{
		snake.push_back(
			Pos{
				moves.front().x,
				moves.front().y
			}
		);
	}
	moves.swap(currentMoves);
	//check if eat
	if(snake[0].x == points[0].x && snake[0].y == points[0].y)
	{
		points.pop_back();
	}
	//check if in wall
	if(snake[0].x >=Consts::mapSize-1 || snake[0].x <= 0 || snake[0].y >=Consts::mapSize-1 || snake[0].y <= 0)
	{
		//todo result screen, add to quit as well
		//result screen
	}
	//check if head in tail
	for(int i=1; i<snake.size(); i++)
	{
		if(snake[0].x == snake[i].x && snake[0].y == snake[i].y)
		{
			//result screen
			break;
		}
	}
}

void Player::draw()
{
	//set origin 

		// //show score

	

	for(int i=0; i<snake.size(); i++)
	{

		if(i==0)
			_map[snake[i].x][snake[i].y] = head;
		else
			_map[snake[i].x][snake[i].y] = tail;
	}
	for(int i=0; i<points.size(); i++)
	{
		_map[points[i].x][points[i].y] = point;
	}
	for(int i=0; i<Consts::mapSize; i++)
	{
		for(int j=0; j<Consts::mapSize; j++)
		{
			//if w border if ' ' empty if O head if o tail if x Point
			
		}		
		//move to next line
	}
	head.setPosition(0,0);
	tail.setPosition(0,0);
	border.setPosition(0,0);
	point.setupPosition(0,0);

}











