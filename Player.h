#ifndef PLAYER_H
#define PLAYER_H

#include "Headers.h"

class Player
{
private:

sf::Shape _map[Consts::mapSize][Consts::mapSize];
std::vector<Pos> snake;
std::vector<Pos> points;

std::queue<Pos> moves;

Pos gamePos;
GameState state;
PlayerMoveSet moveSet;
sf::RenderWindow* mainWindow;

int count;
enum Direction
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};	
Direction direction;


//Map
sf::CircleShape head;
sf::CircleShape tail;
sf::RectangleShape border;
sf::RectangleShape point;
//


void addPoint();
Pos randomPos();

public:
	Player(GameState& gs, PlayerMoveSet pms, Pos p, sf::RenderWindow* rw); //what else?
	~Player();
	void handleInput();
	void update();
	void draw();

	void initialize();


};

#endif