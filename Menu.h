#ifndef MENU_H
#define MENU_H
#include "Headers.h"

class Menu
{
private:

int pos;
std::vector<std::string> menuOptions;
std::vector<sf::Text> menuOptionsText;
sf::Font font;
sf::RenderWindow* mainWindow;
GameState* state;

public:

	Menu(sf::RenderWindow* w, GameState& s);
	~Menu();

	void handleInput();
	void draw();
};



#endif